'use strict';

describe('Filter: presentationFilter Should', function () {

    beforeEach(module('jelatynaSApp'));

    var presentationFilter;

    beforeEach(inject(function ($filter) {
        presentationFilter = $filter('presentationFilter');
    }));

    it('return true if filter string empty', function () {

        var isVisible = presentationFilter({title: "abc"}, "");

        expect(isVisible).toBeTruthy();
    });

    it("return false if filter string not in presentation title", function () {
        var isVisible = presentationFilter(withTitle("a"), "b");

        expect(isVisible).toBeFalsy();
    });


    it("return true if filter string in presentation title", function () {
        var isVisible = presentationFilter(withTitle("abc"), "a");

        expect(isVisible).toBeTruthy();
    });

    it("return true if filter string in presentation title case insensitive", function () {
        var isVisible = presentationFilter(withTitle("Atom"), "a");

        expect(isVisible).toBeTruthy();
    });

    function withTitle(title) {
        return {title: title};
    }

});
