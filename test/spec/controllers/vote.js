'use strict';

xdescribe('Controller: VoteCtrl should', function () {

    var VoteCtrl, scope, voting, modal, deferred;
    var votes = [
        {vote: 0, presentation: {id: 1}}
    ];
    beforeEach(module('jelatynaSApp'));


    beforeEach(inject(function ($controller, $rootScope, _$modal_, $q) {
        scope = $rootScope.$new();
        modal = _$modal_;
        deferred = $q.defer();
        voting = jasmine.createSpyObj("voting", ["next", "prev", "submit", "vote", "getFor", "up", "down"]);
        voting.getFor.and.returnValue(deferred.promise);
        VoteCtrl = $controller('VoteCtrl', {
            $scope: scope,
            voting: voting,
            $modal: _$modal_
        });
    }));

    it('display short description as default', function () {
        expect(scope.configuration.info).toBe("short");
    });

    it('switch to full description on toggle from short', function () {

        scope.toggleInfo();

        expect(scope.configuration.info).toBe(VoteCtrl.info.FULL);
    });


    it('switch to short description on toggle from full', function () {
        withConfiguration(0, VoteCtrl.info.FULL);

        scope.toggleInfo();

        expect(scope.configuration.info).toBe(VoteCtrl.info.SHORT);
    });

    it("load presentations", function () {


        deferred.resolve(votes);
        scope.$apply();

        expect(scope.votes).toEqual(votes);
    });

    it("up-vote presentation", function () {

        scope.up();

        expect(voting.up).toHaveBeenCalled();
    });

    it("down-vote presentation", function () {

        scope.down();

        expect(voting.down).toHaveBeenCalled();
    });


    it("submit and move to next presentation", function () {
        voting.active = true;
        var order = inOrder(voting, "submit", "next");

        scope.next();

        expect(voting.submit).toHaveBeenCalled();
        expect(voting.next).toHaveBeenCalled();
        expect(order).toEqual(["submit", "next"]);
    });

    it("not submit nor move to next if voting inactive", function () {
        voting.active = false;

        scope.next();

        expect(voting.submit).not.toHaveBeenCalled();
        expect(voting.next).not.toHaveBeenCalled();
    });

    it("submit and move to previous presentation", function () {
        voting.active = true;
        var order = inOrder(voting, "submit", "prev");

        scope.prev();

        expect(voting.submit).toHaveBeenCalled();
        expect(voting.prev).toHaveBeenCalled();
        expect(order).toEqual(["submit", "prev"]);
    });

    it("not submit nor move to previous if voting inactive", function () {
        voting.active = false;

        scope.prev();

        expect(voting.submit).not.toHaveBeenCalled();
        expect(voting.prev).not.toHaveBeenCalled();
    });

    it("set 'prev' direction on moving to previous presentation", function () {
        voting.active = true;
        voting.prev.and.returnValue(true);

        scope.prev();

        expect(scope.direction).toBe("prev");
    });

    it("set 'next' direction on moving to next presentation", function () {
        voting.active = true;
        voting.prev.and.returnValue(true);

        scope.next();

        expect(scope.direction).toBe("next");
    });

    it("set 'block' direction if not moved to previous presentation", function () {
        voting.active = true;
        voting.prev.and.returnValue(false);

        scope.prev();

        expect(scope.direction).toBe("block");
    });

    it("open help if not opened yet", function () {
        scope.helpOpened = false;
        mockHelpModal();

        scope.showHelp();

        expect(modal.open).toHaveBeenCalled();
    });

    it("do not open modal if opened already", function () {
        scope.helpOpened = true;
        spyOn(modal, "open");

        scope.showHelp();

        expect(modal.open).not.toHaveBeenCalled();
    });

    it("call toggleInfo on pressing 'm'", function () {
        var event = {keyCode: 77};
        spyOn(scope, 'toggleInfo');
        spyOn(scope, 'up');
        spyOn(scope, 'down');

        scope.$broadcast("keyup", event);

        expect(scope.toggleInfo).toHaveBeenCalled();
        expect(scope.up).not.toHaveBeenCalled();
        expect(scope.down).not.toHaveBeenCalled();
    });

    it("call up on pressing 'up' arrow", function () {
        var event = {keyCode: 38};
        spyOn(scope, 'toggleInfo');
        spyOn(scope, 'up');
        spyOn(scope, 'down');

        scope.$broadcast("keyup", event);

        expect(scope.up).toHaveBeenCalled();
        expect(scope.down).not.toHaveBeenCalled();
        expect(scope.toggleInfo).not.toHaveBeenCalled();
    });

    it("call 'down' on pressing 'down' arrow", function () {
        var event = {keyCode: 40};
        spyOn(scope, 'toggleInfo');
        spyOn(scope, 'up');
        spyOn(scope, 'down');

        scope.$broadcast("keyup", event);

        expect(scope.down).toHaveBeenCalled();
        expect(scope.up).not.toHaveBeenCalled();
        expect(scope.toggleInfo).not.toHaveBeenCalled();
    });

    it("call 'next' on pressing 'right' arrow", function () {
        var event = {keyCode: 39};
        spyOn(scope, 'next');

        scope.$broadcast("keyup", event);

        expect(scope.next).toHaveBeenCalled();
    });

    it("call 'prev' on pressing 'left' arrow", function () {
        var event = {keyCode: 37};
        spyOn(scope, 'prev');

        scope.$broadcast("keyup", event);

        expect(scope.prev).toHaveBeenCalled();
    });

    it("call 'showHelp' on pressing '?'", function () {
        var event = {keyCode: 191};
        spyOn(scope, 'showHelp');

        scope.$broadcast("keyup", event);

        expect(scope.showHelp).toHaveBeenCalled();
    });

    it("not throw exception on pressing unmapped key", function () {

        var keyPressed = function () {
            scope.$broadcast("keyup", {keyCode: -1});
        };

        expect(keyPressed).not.toThrow();
    });



    function withConfiguration(idx, info) {
        info = info || VoteCtrl.info.SHORT;
        scope.configuration.idx = idx;
        scope.configuration.info = info;
    }


    function mockHelpModal() {
        spyOn(modal, "open").and.returnValue({
                opened: {
                    then: function () {
                    }
                },
                result: {
                    then: function () {
                    }
                }
            }
        );
    }

    function inOrder(spy/*, methods*/) {
        var order = [];
        var methods = _.toArray(arguments);
        _.forEach(_.tail(methods), function (method) {
            spy[method].and.callFake(function () {
                order.push(method);
            })
        });
        return order;
    }
});
