'use strict';

describe('Service: Presentations', function () {

  // load the service's module
  beforeEach(module('jelatynaSApp'));

  // instantiate service
  var Presentations;
  beforeEach(inject(function (_Presentations_) {
    Presentations = _Presentations_;
  }));

  it('should do something', function () {
    expect(!!Presentations).toBe(true);
  });

});
