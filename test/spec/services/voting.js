'use strict';

xdescribe('Service: voting should', function () {

    var voting, vote, http;
    beforeEach(module('jelatynaSApp'));


    beforeEach(inject(function (_voting_, _Vote_, $httpBackend) {
        voting = _voting_;
        vote = _Vote_;
        spyOn(vote, "save");
        spyOn(vote, "get").and.callThrough();
        http = $httpBackend;
        voting.items = [
            {vote: 0, presentation: {id: 1}},
            {vote: 0, presentation: {id: 2}},
            {vote: 0, presentation: {id: 3}}
        ];
    }));

    it('move to 2nd presentation', function () {

        voting.next();

        expect(voting.idx).toBe(1);
    });

    it('move to 3rd presentation', function () {
        voting.next();

        voting.next();

        expect(voting.idx).toBe(2);
    });


    it('move back to 2nd presentation', function () {
        withIdx(2);

        var move = voting.prev();

        expect(voting.idx).toBe(1);
        expect(move).toBeTruthy();
    });

    it('move back to 1st presentation', function () {
        withIdx(1);

        var move = voting.prev();

        expect(voting.idx).toBe(0);
        expect(move).toBeTruthy();
    });

    it('not move back if displaying first presentation', function () {
        withIdx(0);

        var move = voting.prev();

        expect(voting.idx).toBe(0);
        expect(move).toBeFalsy();
    });


    it("be current", function () {
        withIdx(0);

        var current = voting.isCurrent(0);

        expect(current).toBeTruthy();
    });

    it("not be current", function () {
        withIdx(1);

        var current = voting.isCurrent(2);

        expect(current).toBeFalsy();

        expect(["a", "b",{name: "test"}],1).not.toContain(1);
    });


    it("inactivate if moving past last presentation", function () {
        withIdx(2);

        voting.next();

        expect(voting.active).toBeFalsy();
    });


    it("change vote for 1st presentation", function () {
        withIdx(0);

        voting.vote(-1);

        expect(voteAt(0)).toBe(-1);
    });

    it("change vote for 2nd presentation", function () {
        withIdx(1);

        voting.vote(1);

        expect(voteAt(1)).toBe(1);
    });

    it("up-vote presentation", function () {
        withIdx(1);
        voting.vote(0);

        voting.up();

        expect(voteAt(1)).toBe(1);
    });

    it("up-vote presentation from 1 to -1 ", function () {
        withIdx(0);
        voting.vote(1);

        voting.up();

        expect(voteAt(0)).toBe(-1);
    });

    it("down-vote presentation", function () {
        withIdx(1);
        voting.vote(0);

        voting.down();

        expect(voteAt(1)).toBe(-1);
    });
    it("down-vote presentation from -1 to 1", function () {
        withIdx(1);
        voting.vote(-1);

        voting.down();

        expect(voteAt(1)).toBe(1);
    });


    it("submit vote for 1st presentation", function () {

        withIdx(0);
        withVote(-1);
        voting.key = "SESSION_KEY";

        voting.submit();

        expect(vote.save).toHaveBeenCalledWith({key: "SESSION_KEY", vote: -1, presentationId: 1});
    });

    it("submit vote for 2nd presentation", function () {
        withIdx(1);
        withVote(1);
        voting.key = "SESSION_KEY_2";

        voting.submit();

        expect(vote.save).toHaveBeenCalledWith({key: "SESSION_KEY_2", vote: 1, presentationId: 2});
    });

    it("get presentations for session key", function () {
        var presentations = [
            {id: 1}
        ];
        http.whenGET(/.*/).respond({votes: presentations});

        voting.getFor("key");
        http.flush();

        expect(vote.get).toHaveBeenCalledWith({key: "key"}, jasmine.any(Function));
        expect(voting.items).toEqual(presentations);
    });

    it("save session key", function () {
        http.whenGET(/.*/).respond({key: "my-key", votes: []});

        voting.getFor("");
        http.flush();

        expect(voting.key).toBe("my-key");
    });

    function withIdx(idx) {
        idx = idx || 0;
        voting.idx = idx;
    }

    function voteAt(idx) {
        return voting.items[idx].vote;
    }

    function withVote(vote) {
        voting.vote(vote);
    }


});
