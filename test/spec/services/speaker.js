'use strict'

describe('Service: Speaker', function () {
    var Speaker = {};

//   load the service's module
    beforeEach(module('jelatynaSApp'));

//  instantiate service
    beforeEach(inject(function(_Speaker_) {
        Speaker = _Speaker_;
    }));


    it('should do something', function () {
        expect(!!Speaker).toBe(true);
    });

});
