'use strict'
var express = require('express');
var fs = require('fs');
var app = express();


app.get("/api/speakers", function (req, res) {
    res.json(getDataFor("speakers"));
});
app.get("/api/hosts/main", function (req, res) {
    res.json(getDataFor("hosts"));
});
app.get("/api/hosts/volunteers", function (req, res) {
    res.json(getDataFor("hosts"));
});
app.get('/api/agenda', function (req, res) {
    res.json(getDataFor("agenda"));
});
app.get('/api/v4p/:key', function (req, res) {
    res.json(getDataFor("v4p"));
});

app.get('/api/news/:page/:pageSize', function (req, res) {
    var page = parseInt(req.params.page);
    var size = parseInt(req.params.pageSize);
    console.log(["page",page, "size", size, "start", page*size, "end",page * size + size]);
    var news = getDataFor("news").slice(page * size, page * size + size);
    console.log(news.length);
    res.json(news);
});
app.get('/api/sponsors', function (req, res) {
    res.json(getDataFor("sponsors"));
});
app.get('/api/pages/:name', function (req, res) {
    res.json(getDataFor("pages")[req.params.name]);
});
app.get('/api/presentations', function (req, res) {
    res.json(getDataFor("presentations"));
});

app.get('/api/register', function (req, res) {
    res.json({
        "presentations": getDataFor("presentations")
    });
});

app.post('/api/v4p/:key', express.bodyParser(), function (req, res) {
    console.log("saving " + JSON.stringify(req.body));
    res.send(200);
});

function getDataFor(type) {
    return JSON.parse(fs.readFileSync(__dirname + "/" + type + ".json", 'utf8'));
}

module.exports = app