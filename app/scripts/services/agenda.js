'use strict';

angular.module('jelatynaSApp')
    .factory('Agenda', function ($resource) {
        return $resource('/agenda');
    });
