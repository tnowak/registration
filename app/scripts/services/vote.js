'use strict';

angular.module('jelatynaSApp')
    .factory('Vote', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer+'/v4p/:key', {'key': '@key'}, {});
    }]);
