'use strict';

angular.module('jelatynaSApp')
    .factory('Hosts', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/hosts/:type', {'type': '@type'}, {});
    }
    ])
;
