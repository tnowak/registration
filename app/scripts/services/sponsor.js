'use strict';

angular.module('jelatynaSApp')
    .factory('Sponsor', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/sponsors');
    }]);
