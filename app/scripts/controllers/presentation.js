'use strict';

angular.module('jelatynaSApp')
  .controller('PresentationCtrl', function ($scope, Presentations) {
    $scope.presentations = Presentations.query();
  });
