'use strict';

angular.module('jelatynaSApp')
    .controller('AgendaCtrl', function ($scope, Agenda) {
        $scope.presentationFilter = '';
        $scope.visibleRooms = [];

        $scope.agenda = Agenda.get(function () {
            $scope.agenda.visibleRooms = $scope.agenda.rooms;
        });


        $scope.getAllPresentationsForSlot = function (slotId) {
            return  _.chain(findSlotBy(slotId).presentations)
                .filter(function (presentationInRoom) {
                    return $scope.agenda.visibleRooms.indexOf(presentationInRoom.room) !== -1;
                })
                .map(function (item) {
                    return item.presentation;
                })
                .value();
        };

        $scope.changed = function (name, selected) {
            var agenda = $scope.agenda;
            if (selected) {
                agenda.visibleRooms.push(name);
                agenda.visibleRooms = _.sortBy(agenda.visibleRooms, function (room) {
                    return $scope.agenda.rooms.indexOf(room);
                });
            }
            else {
                agenda.visibleRooms = _.reject(agenda.visibleRooms, function (room) {
                    return room === name;
                });
            }
        };

        $scope.isVisible = function (room) {
            return _.contains($scope.agenda.visibleRooms, room);
        };

        function findSlotBy(id) {
            return _.find($scope.agenda.schedule, function (item) {
                return item.slotId === id;
            });
        }

    });