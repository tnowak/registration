'use strict';

angular.module('jelatynaSApp')
    .controller('PartnersCtrl', function ($scope, Sponsor) {
        $scope.types = [
            {id: "gold", name: "Złoci"},
            {id: "silver", name: "Srebrni"},
            {id: "bronze", name: "Brązowi"},
            {id: "technical", name: "Techniczni"} ,
            {id: "press", name: "Medialni"}

        ];

        $scope.active = $scope.types[0];
        $scope.partners = Sponsor.get();
        $scope.activate = function ($event, type) {
            $event.preventDefault();
            $scope.active = type;
        };

        $scope.isVisible = function (type) {
            var partners = $scope.partners[type.id];
            return partners && partners.length;

        };
    });
