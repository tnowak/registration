'use strict';
angular.module('jelatynaSApp')
    .controller('VoteCtrl', function ($scope, voting, $modal, hotkeys) {
        var info = this.info = {SHORT: 'short', FULL: 'full'};

        hotkeys.add({
            persistent: false,
            combo: 'alt+up',
            description: 'Łapka w górę!',
            callback: function (event) {
                callback(event, "up");
            }
        });
        hotkeys.add({
            persistent: false,
            combo: 'alt+down',
            description: 'Łapka w dół!',
            callback: function (event) {
                callback(event, "down");
            }
        });
        hotkeys.add({
            persistent: false,
            combo: 'alt+right',
            description: 'Następna prezentacja',
            callback: function (event) {
                callback(event, "next");
            }
        });
        hotkeys.add({
            persistent: false,
            combo: 'alt+left',
            description: 'Poprzednia prezentacja',
            callback: function (event) {
                callback(event, "prev");
            }
        });
        hotkeys.add({
            persistent: false,
            combo: 'm',
            description: 'Krótki/Pełny opis prezentacji',
            callback: function (event) {
                callback(event, "toggleInfo");

            }
        });
        hotkeys.add({
            persistent: false,
            combo: 'i',
            description: 'Informacja o prelegencie',
            callback: function (event) {
                callback(event, "open");
            }
        });


        $scope.configuration = { info: 'short', idx: 0};

        $scope.votes = [];
        voting.get().then(function (votes) {
            $scope.votes = votes;
        });

        $scope.isVisible = function (idx) {
            return voting.isCurrent(idx);
        };
        $scope.isActive = function () {
            return voting.isActive();
        };

        $scope.next = function () {
            submit(function () {
                voting.next();
                $scope.direction = 'next';
            });
        };

        $scope.prev = function () {
            submit(function () {
                var moved = voting.prev();
                $scope.direction = moved ? 'prev' : 'block';
            });
        };

        $scope.toggleInfo = function () {
            if ($scope.configuration.info === info.SHORT) {
                $scope.configuration.info = info.FULL;
            } else {
                $scope.configuration.info = info.SHORT;
            }
        };

        $scope.up = function () {
            voting.up();
        };

        $scope.down = function () {
            voting.down();
        };
        $scope.vote = function (vote) {
            voting.vote(vote);
        };

        $scope.showHelp = function () {
            if ($scope.helpOpened === false) {
                var instance = $modal.open({
                    templateUrl: 'help-modal.html',
                    scope: $scope
                });
                instance.opened.then(function () {
                    $scope.helpOpened = true;
                });
                instance.result.then(null, function () {
                    $scope.helpOpened = false;
                });
            }
        };
        $scope.notStarted = function () {
            return voting.notStarted();
        };
        $scope.start = function () {
            return voting.start();
        };
        $scope.currentIdx = function () {
            return voting.idx + 1;
        };

        $scope.open = function () {
            $scope.$broadcast("open");
        };

        $scope.helpOpened = false;

        function submit(callback) {
            $scope.saving = true;
            voting.submit()
                .then(function () {
                    $scope.saving = false;
                    callback();
                });
        }

        function callback(event, method) {
            if ($scope.isActive()) {
                event.preventDefault();
                $scope[method]();
            }
        }


    });
