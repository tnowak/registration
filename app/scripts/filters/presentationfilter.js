'use strict';

angular.module('jelatynaSApp')
    .filter('presentationFilter', function () {
        return function (input, filter) {
            return input.title.toLocaleLowerCase().indexOf(filter.toLowerCase()) !== -1;
        };
    });
